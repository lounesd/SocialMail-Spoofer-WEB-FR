# SocialMail-Spoofer-WEB-FR
SocialMail-Spoofer est un outil open-source conçu pour des fins éducatives et de sensibilisation aux risques de sécurité liés à l'ingénierie sociale et au spoofing d'adresses e-mail.

Note importante : Ce projet doit être utilisé de manière responsable et éthique, en respectant les lois et réglementations locales concernant l'utilisation de technologies similaires. Il ne doit en aucun cas être utilisé à des fins malveillantes ou illégales.

!! Je ne fournis pas de serveur SMTP. Vous pouvez en créer un en utilisant, par exemple, Postfix.!! Je ne recommande pas l'utilisation de SMTP ne vous appartenant pas.
SMTP à modifier dans le .js

Ce projet permet d'envoyer des courriels tout en permettant de personnaliser l'expediteur, mise en évidence d'une des vulnérabilités du courrier électronique. Disponible en deux versions : en langage Bash ou en site web (HTML, CSS, JavaScript).

Version Bash : https://gitlab.com/lounesd/SocialMail-Spoofer-FR
